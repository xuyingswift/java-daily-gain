interface CustomFunctionalInterfaces {
    //What is a functional interface?
    //an interface which has only one single abstract method.

    //What is the purpose of Functional interface?
    // the most important use of functional interface is that implementation of their abstract method
    // can be passed around as lambda expressions.
    // functional interfaces primarily enable behavior parameterization

    //@FunctionalInterface annotation can be used to explicitedly specify that a given interface is to be
    //treated as a functional interface

    // Thia is the only abstract method
    //Single abstract method
    void firstMethod();
}
