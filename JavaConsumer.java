import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

public class JavaConsumer {
    // Consumer<T> is a built-in functional interface.
    // these examples will show accept() and andThen() methods of Consumer interface

    //What is java.util.function.Consumer
    // Consumer can be used in all contexts where an object needs to be consumed
    //such as: taken as input; some operation is to be performed on the object without
    // returning any result.

    //What is a functional interface, an interface only have one abstract method
    // it can be used as assignment target for a lambda expression
    // or a method reference

    //Advantage of predefined java util.function.consumer
    //In all scenarios where an object is to be taken as input and an operation performed on it
    // Consumer<T> can be used without the need to define a new functional interface

 /*   @FunctionalInterface
    public interface Consumer<T> { // Consumer has been define with the generic T
        void accept(T t);
        default Consumer<T> andThen(Consumer<? super T> after) {
            Objects.requireNonNull(after);
            return (T t)  -> { accept(t); after.accept(t);};
        }
    }*?
  */
    public static void main(String[ ] args){
        // Define an instance of Consumer<Integer> using a lambda expression
        Consumer<Integer> consumer = i -> System.out.println(" " + i);
        // Now we have created the second consumer interface based on the first one
        Consumer<Integer> consumerWithAndThen = consumer.andThen(i -> System.out.println("(printed " +i + ")"));
        List<Integer> integersList = Arrays.asList(new Integer(1), new Integer(10),
                                                    new Integer(100), new Integer(-10));
        printList(integersList, consumer);
        printList(integersList, consumerWithAndThen);
    }
    // the printList method takes 2 input an instance of Consumer interface which contains the
    // printing logic and the list which is to be printed.
    // iterates through the list of integers and invokes the accept() method of the consumer object
    // for every integer in the list
    public static void printList(List<Integer> listOfIntegers, Consumer<Integer> consumer) {
        for(Integer integer : listOfIntegers) {
            //The accept() method which works as per the lambda definition assigned to the consumer interface
            // the accept() takes input type T and returns no value
            consumer.accept(integer);
        }
    }



}
