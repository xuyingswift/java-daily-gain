import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class generics {
    public static void main(String[ ] args) {
        List list = new LinkedList();
        list.add(new Integer(1));
        // the complier will need to know the return type
        // we are getting the list to return the last item in the list
        Integer i = (Integer) list.iterator().next();
        System.out.println(i);
        System.out.println("Casting can cause type-related problem");
        System.out.println(" It is much easier if programmers could express their intention of using specific types and the compiler can ensure the correctness of such typey");
        System.out.println("This is the core idea behind Generics avoiding the type errors");

        //<> the diamond operator contains the type, we narrow the specialization, we specify the type that will be held in the list,
        // so compiler can enforce the type at compile time, this can make your program robust
        List<Integer> list1 = new LinkedList<>();
        System.out.println("Generic Methods");
        System.out.println("Generic Methods are those methods that are written with a single method declaration can be called with arguments of diffrent type");


    }
    // generic method has parameter before the return type of the method
    // type parameter can be bounded
    // generic mthods can have different type parmeters separated by commas in the method signature
    //method body for a generic method is just like a normal method
    public <T> List<T> fromArrayToList(T[] a) {
        return Arrays.stream(a).collect(Collectors.toList()) ;
    }
    //to deal with more one generic types
    public static <T, G> List<G> fromArrayToList(T[] a, Function<T, G> mapperFunction) {
        return Arrays.stream(a)
                     .map(mapperFunction)
                     .collect(Collectors.toList());
    }

    //Bounded Generics, type parameters can be bounded. Bounded means "restricted". we can restrict types that can be accepted by a method
    // subclasses or supper classes


}
