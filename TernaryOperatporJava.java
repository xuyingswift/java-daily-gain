public class TernaryOperatporJava {
    // ?: is a ternary operator that is part of the syntax basic conditional expressions in several programming languages
    // It is commonly referred to as the conditional operator
    // An expression a ? b : c evaluates to b if value of a is true and otherwise to c
    // variable = condition ? value_if_true : value_if_false
    public static void main(String[ ] args){
        int num = 20;
        numbers(num);
        numbers(4);
    }

    public static void numbers(int num) {
        String msg = "";
        if(num > 10) {
            msg = "Number is greater than 10";
            System.out.println(msg);
        }else {
            msg = "Number is less than or equal to 10";
            System.out.println(msg);
        }

        // we can change this code block into ternary operator
        System.out.println();
        System.out.println("Using the ternary operator ?:");
        final String msg1 = num > 10
            ? "Number is greater than 10"
            : "Number is less than 10";
        System.out.println(msg1);

        System.out.println("Nesting Ternary Operator");
        String msg2 = num > 10 ? "Number is greater than 10" : num > 5
            ? "Number is greater than 5 " : "Number is less than or equal to 5";
        System.out.println(msg2);
    }

}
