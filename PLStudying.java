import java.math.BigDecimal;

public class PLStudying {
    public static void main(String[] args){
        // Java compareTo method
        // if obj1 > obj2, return positive number
        // if obj1 < obj2, return negative number
        // if obj1 = obj2. return 0
        String s1 = "Hello";
        String s2 = "big";
        System.out.println(s1.compareTo(s2));

        BigDecimal num1 = new BigDecimal(5.4446);
        BigDecimal num2 = new BigDecimal(4.6667);
        System.out.println(num1.compareTo(num2));

        // BigDecimal class provides operations on double numbers for arithmetic, scale handling, rounding, comparison,
        // format conversion and hasing. it can handling very large or very small floating point numbers with great
        // precision but compensating with the time complexity a bit.

        double a = 0.03;
        double b = 0.04;
        double c = b - a;
        System.out.println(c);

        BigDecimal a1 = new BigDecimal(0.03);
        BigDecimal b1 = new BigDecimal(0.04);
        BigDecimal c1 = b1.subtract(a1);
        System.out.println(c1);

    }
}
